#!/bin/bash

source /home/service/service_flask/bin/activate

exec uwsgi --ini /home/service/service_flask/env/uwsgi.ini --py-autoreload 1
